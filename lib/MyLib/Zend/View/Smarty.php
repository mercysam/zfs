<?php

class MyLib_Zend_View_Smarty extends Zend_View_Abstract
{
    private $_smarty;

    public function __construct() {
		$config = Zend_Registry::get('config');
		
        require_once $config->smarty->path . 'Smarty.class.php';
        
        $this->_smarty = new Smarty();
        $this->_smarty->template_dir    = $config->smarty->template_dir;
        $this->_smarty->left_delimiter  = $config->smarty->left_delimiter;
        $this->_smarty->right_delimiter = $config->smarty->right_delimiter;
        $this->_smarty->compile_dir     = $config->smarty->compile_dir;
        $this->_smarty->config_dir      = $config->smarty->config_dir;
        $this->_smarty->cache_dir       = $config->smarty->cache_dir;
        $this->_smarty->cache_lifetime  = $config->smarty->cache_lifetime;
        $this->_smarty->caching         = $config->smarty->caching;
        $this->_smarty->compile_check   = $config->smarty->compile_check;
        $this->_smarty->plugins_dir     = array('plugins', $config->smarty->my_plugins_dir);
		
		$this->assign('ZendView'  , $this);
        $this->assign('ZendLayout', $this->layout());
    }

    public function getEngine() {
        return $this->_smarty;
    }

    public function __set($key, $val) {
        $this->_smarty->assign($key, $val);
    }

    public function __get($key) {
        return $this->_smarty->get_template_vars($key);
    }

    public function __isset($key) {
        return $this->_smarty->get_template_vars($key) != null;
    }

    public function __unset($key) {
        $this->_smarty->clear_assign($key);
    }

    public function assign($spec, $value=null) {
        if (is_array($spec)) {
          $this->_smarty->assign($spec);
          return;
        }
        $this->_smarty->assign($spec, $value);
    }

    public function clearVars() {
        $this->_smarty->clear_all_assign();
    }

    public function render($name) {
        return $this->_smarty->fetch(strtolower($name));
    }

    public function _run() {}
}
