<?php
define('ROOT_PATH', realpath(dirname(__FILE__) . '/../') . '/' );
define('APP_PATH' , ROOT_PATH . 'application/');
define('LIB_PATH' , ROOT_PATH . 'lib/');

$path = array(
	LIB_PATH,
	APP_PATH.'models',
	get_include_path()
);
set_include_path(implode(PATH_SEPARATOR, $path));


require_once 'Zend/Loader.php';
Zend_Loader::registerAutoload();


$config = new Zend_Config_Ini(APP_PATH.'config/config.ini', 'development');
Zend_Registry::set('config', $config);


$viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer();
$viewRenderer->setView(new MyLib_Zend_View_Smarty());
$viewRenderer->setViewSuffix('tpl');
Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);


$controller = Zend_Controller_Front::getInstance();
$controller->setControllerDirectory(APP_PATH . 'controllers');
$controller->throwExceptions(true);
$controller->dispatch();




function _($a = 'test ok'){
    $argsNum = func_num_args();
    if( $argsNum == 0 ){
        $a = 'test ok';
    } else if( $argsNum > 1 ){
        $a = func_get_args();
    }

    echo '<pre>';print_r($a);die;
}

function __($a = 'test ok'){
    $argsNum = func_num_args();
    if( $argsNum == 0 ){
        $a = 'test ok';
    } else if( $argsNum > 1 ){
        $a = func_get_args();
    }

    echo '<pre>';print_r($a);
}